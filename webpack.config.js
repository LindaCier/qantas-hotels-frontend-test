const common = require('./webpack.common.js');

const cssLoader = common.CommonLoaders.css(false);
const pngLoader = common.CommonLoaders.png;
const jsLoader = common.CommonLoaders.js;
const jsonLoader = common.CommonLoaders.json;
const output = common.CommonLoaders.output(__dirname);

module.exports = {
    target: 'web',
    devtool: 'source-map',
    entry: './src/index.js',
    output,
    resolve: {
        modules: ['node_modules'],
    },
    module: {
        rules: [jsLoader, jsonLoader, pngLoader, cssLoader.loader],
        noParse: /\.min\.js/,
    },
    devServer: {
        historyApiFallback: true,
        open: true,
    },
};

const path = require('path');

module.exports.CommonLoaders = {};
module.exports.CommonLoaders.css = (isProd) => {
    const minMode = isProd ? '&minimize' : '';
    return {
        loader: {
            test: /\.less$/,
            loaders: [
                'style-loader',
                `css-loader?importLoaders=1${minMode}`,
                'postcss-loader',
                'less-loader',
            ],
        },
    };
};

module.exports.CommonLoaders.png = {
    test: /\.(jpg|png)$/,
    exclude: /node_modules\/[^@]/,
    use: [
        {
            loader: 'url-loader',
        },
    ],
};

module.exports.CommonLoaders.json = {
    test: /\.json$/,
    exclude: /node_modules\/[^@]/,
    use: [
        {
            loader: 'json-loader',
        },
    ],
};

module.exports.CommonLoaders.js = {
    test: /\.$|\.js$|\.jsx$|\.es6$/,
    exclude: /node_modules\/[^@]/,
    use: [
        {
            loader: 'babel-loader',
        },
    ],
};

module.exports.CommonLoaders.output = (__dirname) => ({
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/',
    library: '[name]_lib',
});

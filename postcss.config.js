const autoprefixer = require('autoprefixer');

module.exports = {
    plugins: [
        autoprefixer({
            browsers: [
                'last 2 versions',
                'IE >= 10',
                'Chrome >= 31',
                'Android >= 4.4',
                'Safari >= 7',
                'iOS >= 7',
                'Firefox >= 27',
            ],
        }),
    ],
};
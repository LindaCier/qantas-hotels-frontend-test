// NPM Modules
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import HotelReducer from './components/Hotel/Hotel-reducer';
import HotelSaga from './components/Hotel/Hotel-sagas';

// Import APP
import App from './container/App';

// Name
const name = 'test-app';

// Store
const devToolExt = window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f;
const store = createStore(
    HotelReducer,
    compose(
        applyMiddleware(createSagaMiddleware(HotelSaga)),
        devToolExt,
    ),
);

// Render
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'),
);

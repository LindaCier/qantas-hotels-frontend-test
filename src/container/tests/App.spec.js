import { expect } from 'chai';

// Functions being tested
import App from '../App';

describe('Given App container is rendered', () => {
    context('When App component is called', () => {
        it(`Then the app wrap has the class 'test__app'`, () => {
            const result = <App />;
            const wrapper = shallow(result);

            expect(wrapper.hasClass('test__app')).to.equal(true);
        });
    });
});

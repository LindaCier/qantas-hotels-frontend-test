// NPM Modules
import React, { Component } from 'react';

// Import Components
import Hotel from '../components/Hotel/Hotel';

// Styles
import './App.less';

export default class App extends Component {
    render() {
        return (
            <div className="test__app">
                <Hotel />
            </div>
        );
    }
}

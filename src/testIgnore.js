// This file gets required into mocha.opts of the consuming module, and is used
// to stub any non-js files from being interpreted by the test runner

var requireHacker = require('require-hacker');

var ignoreExtensions = ['sass', 'scss', 'less', 'gif', 'jpg', 'svg', 'png'];

ignoreExtensions.forEach((type) => {
    requireHacker.hook(type, () => 'module.exports = ""');
});

// Fetch API;

export const fetchHotelData = () => {
    const url = '/data/data.json';

    return fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json().then((data) => {
                    return data.results;
                });
            }

            return undefined;
        })
        .catch(() => Promise.reject());
};

// Imports
import { put } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { fork } from 'redux-saga/effects';

// Actions
import * as HotelActions from './Hotel-actions';

// Action Types
import * as actionTypes from './Hotel-actionTypes';

// APIs
import { fetchHotelData } from './Hotel-api';

// Gets the list of Hotel and set them in the store.
export function* loadHotelData() {
    try {
        const Hotel = yield fetchHotelData();
        yield put(HotelActions.HotelDetailsReceived(Hotel));
    } catch (e) {
        console.log(e);
    }
}

// Create watchers that listen to sagas
export function* dataWatchers() {
    yield [takeLatest(actionTypes.GET_Hotel, loadHotelData)];
}

// Export the HotelSaga
export default function* HotelSaga() {
    yield [fork(dataWatchers)];
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as HotelActions from './Hotel-actions';
import HotelList from '../HotelList/HotelList';
import Qantas from './assets/Qantas.png';
import './Hotel.less';

export class Hotel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectValue: 'highToLow',
        };
    }

    componentDidMount() {
        this.props.getHotel();
    }

    sortByPriceDesc = (hotels) => {
        const descHotels = hotels.sort((a, b) => {
            /* istanbul ignore next */
            if (a.offer.displayPrice.amount > b.offer.displayPrice.amount) return -1;
            else if (a.offer.displayPrice.amount < b.offer.displayPrice.amount) return 1;
            else return 0;
        });

        return descHotels;
    };

    sortByPriceAsc = (hotels) => {
        /* istanbul ignore next */
        const ascHotels = hotels.sort((a, b) => {
            if (a.offer.displayPrice.amount < b.offer.displayPrice.amount) return -1;
            else if (a.offer.displayPrice.amount > b.offer.displayPrice.amount) return 1;
            else return 0;
        });

        return ascHotels;
    };

    handleSelectChange = (e) => {
        this.setState({
            selectValue: e.target.value,
        });
    };

    render() {
        const { hotels } = this.props;
        const { selectValue } = this.state;
        const hotelsDisplay =
            selectValue === 'highToLow'
                ? this.sortByPriceDesc(hotels)
                : this.sortByPriceAsc(hotels);

        const logoStyle = {
            backgroundImage: `url(${Qantas})`,
            backgroundPosition: 'center center',
            backgroundSize: '150px auto',
            backgroundRepeat: 'no-repeat',
        };

        return (
            <div className="test__Hotel">
                <div className="test__Hotel--logo" style={logoStyle} />
                <div className="test__Hotel--numbers">
                    <span className="__number-bold">{hotels.length}</span> of hotels in
                    <span className="__number-bold"> Sydeney</span>.
                </div>

                <div className="test__Hotel--sort">
                    Sort By{' '}
                    <select
                        className="test__Hotel--select"
                        value={this.state.selectValue}
                        onChange={(e) => this.handleSelectChange(e)}
                    >
                        <option value="highToLow">Price high-low</option>
                        <option value="LowToHigh">Price low-high</option>
                    </select>
                </div>
                <HotelList hotels={hotelsDisplay} className="test__Hotel--List" />
            </div>
        );
    }
}

Hotel.protoTypes = {
    getHotel: PropTypes.func.isRequired,
    hotels: PropTypes.array.isRequired,
};

Hotel.defaultProps = {
    hotels: [],
};

/* istanbul ignore next */
const mapStateToProps = (state, props) => ({
    ...state.Hotel,
    hotels: state.Hotel,
});

const mapDispatchToProps = {
    ...HotelActions,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Hotel);

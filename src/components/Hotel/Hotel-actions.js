// Import action types
import * as actionTypes from './Hotel-actionTypes';

export const getHotel = () => ({
    type: actionTypes.GET_Hotel,
});

export const HotelDetailsReceived = (Hotel) => ({
    type: actionTypes.Hotel_DETAILS_RECEIVED,
    Hotel,
});

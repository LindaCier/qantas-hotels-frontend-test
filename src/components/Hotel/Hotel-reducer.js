// Import action types
import * as actionTypes from './Hotel-actionTypes';

// setup Initial State
export const initialState = {
    Hotel: [],
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case actionTypes.Hotel_DETAILS_RECEIVED: {
            return {
                ...state,
                Hotel: action.Hotel,
            };
        }

        default: {
            return state;
        }
    }
};

import { expect } from 'chai';

// Functions being tested
import { Hotel } from '../Hotel';

const hotelsList = [
    {
        id: 'cxd650nuyo',
        property: {
            propertyId: 'P107801',
            title: 'Courtyard by Marriott Sydney-North Ryde',
            address: ['7-11 Talavera Rd', 'North Ryde'],
            previewImage: {
                url: 'https://unsplash.it/145/125/?random',
                caption: 'Image of Courtyard by Marriott Sydney-North Ryde',
                imageType: 'PRIMARY',
            },
            rating: {
                ratingValue: 4.5,
                ratingType: 'self',
            },
        },
        offer: {
            promotion: {
                title: 'Exclusive Deal',
                type: 'MEMBER',
            },
            name: 'Deluxe Balcony Room',
            displayPrice: {
                amount: 329.0,
                currency: 'AUD',
            },
            savings: {
                amount: 30.0,
                currency: 'AUD',
            },
            cancellationOption: {
                cancellationType: 'NOT_REFUNDABLE',
            },
        },
    },
    {
        id: 'mesq6mggyn',
        property: {
            propertyId: 'P107802',
            title: 'Primus Hotel Sydney',
            address: ['339 Pitt St', 'Sydney'],
            previewImage: {
                url: 'https://unsplash.it/145/125/?random',
                caption: 'Image of Primus Hotel Sydney',
                imageType: 'PRIMARY',
            },
            rating: {
                ratingValue: 5,
                ratingType: 'self',
            },
        },
        offer: {
            promotion: {
                title: 'Exclusive Deal',
                type: 'MEMBER',
            },
            name: 'Deluxe King',
            displayPrice: {
                amount: 375.0,
                currency: 'AUD',
            },
            savings: {
                amount: 28.0,
                currency: 'AUD',
            },
            cancellationOption: {
                cancellationType: 'FREE_CANCELLATION',
            },
        },
    },
];

describe('Hotel component test suite', () => {
    context('When Hotel component is rendered', () => {
        it('should render without crashing', () => {
            const getHotelStub = sinon.stub();
            const result = <Hotel getHotel={getHotelStub} />;
            const wrapper = shallow(result);
            expect(wrapper).to.be.defined;
            expect(wrapper.props()).to.be.defined;
        });
        it('should has one class called "test__Hotel"', () => {
            const getHotelStub = sinon.stub();
            const result = <Hotel getHotel={getHotelStub} />;
            const wrapper = shallow(result);
            expect(wrapper.hasClass('test__Hotel')).to.equal(true);
        });
        it('should render one elements called "test__Hotel--List"', () => {
            const getHotelStub = sinon.stub();
            const result = <Hotel getHotel={getHotelStub} />;
            const wrapper = shallow(result);

            expect(wrapper.find('.test__Hotel--List')).to.have.length(1);
        });
        it('should call getHotel', () => {
            const getHotelSpy = sinon.spy();
            const result = <Hotel getHotel={getHotelSpy} />;
            let wrapper = mount(result);
            wrapper.instance();

            expect(getHotelSpy).to.have.been.called;
        });
        it('should render two classes called "test__List--hotel"', () => {
            const getHotelStub = sinon.stub();
            const result = <Hotel hotels={hotelsList} getHotel={getHotelStub} />;
            const wrapper = mount(result);

            expect(wrapper.find('.test__List--hotel')).to.have.length(2);
        });
    });
    context('When Select option has been chosen', () => {
        it('should change the select option value', () => {
            const getHotelStub = sinon.stub();
            const result = <Hotel getHotel={getHotelStub} />;
            const wrapper = mount(result);
            wrapper
                .find('.test__Hotel--select')
                .simulate('change', { target: { value: 'lowToHigh' } });
            expect(wrapper.find('.test__Hotel--select').prop('value')).to.equal('lowToHigh');
        });
    });
});

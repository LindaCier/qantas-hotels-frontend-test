import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './HotelList.less';

class HotelList extends Component {
    render() {
        const { hotels } = this.props;
        const HotelList = hotels.map((hotel, index) => {
            const saveAmount =
                hotel.offer.savings === null ? '' : `Save $${hotel.offer.savings.amount}~`;
            const ratingStyle = {
                ['--rating']: `${hotel.property.rating.ratingValue}`,
            };
            return (
                <div key={index} className="test__List--hotel">
                    <div className="__hotel--image">
                        <img src={hotel.property.previewImage.url} />
                        <span className="__hotel--promotion">{hotel.offer.promotion.title}</span>
                    </div>
                    <div className="__hotel--details">
                        <div className="__hotel--info">
                            <div className="__hotel--location">
                                <div className="__hotel--title">
                                    {hotel.property.title}
                                    <span
                                        className={hotel.property.rating.ratingType}
                                        style={ratingStyle}
                                    ></span>
                                </div>
                                <div className="__hotel--adress">
                                    {hotel.property.address[0]} {hotel.property.address[1]}
                                </div>
                            </div>
                            <div className="__hotel--offer">{hotel.offer.name}</div>
                            <div className="__hotel--cancel">
                                {hotel.offer.cancellationOption.cancellationType}
                            </div>
                        </div>
                        <div className="__hotel--price">
                            <div className="__hotel--currency">
                                1 night total ({hotel.offer.displayPrice.currency})
                            </div>
                            <div className="__hotel--amount">
                                <span className="__amount--sign">$</span>
                                {hotel.offer.displayPrice.amount}
                            </div>
                            <div className="__hotel--save">{saveAmount}</div>
                        </div>
                    </div>
                </div>
            );
        });

        return <div>{HotelList}</div>;
    }
}

HotelList.propTypes = {
    hotel: PropTypes.array,
};

export default HotelList;
